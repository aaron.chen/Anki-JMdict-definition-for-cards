# -*- coding: utf-8 -*-

import re
from bs4 import BeautifulSoup

'''
dictionary_def.py
A (quick and dirty) python script to fill in Japanese anki cards with JMdict definition
'''

from anki import Collection
from anki.importing import TextImporter
import csv, os, re

# change to path of your JMdict_e file (may need to change to JMdict_e.xml)
path_to_jmdict = 'C:/Users/UserName/AppData/Roaming/Anki2/dev/JMdict_e' 
# change to path of your anki collection file
collection_path = "C:/Users/UserName/AppData/Roaming/Anki2/Anki_Username/collection.anki2"
# change anki search query to which cards you want to change (e.g. within a certain deck, note type, card with certain field, etc.)
search_query = u'deck:日本語'
# For cards within that query, this is the field that contains the kanji word, and will be used to search the dictionary (e.g. the field that contains "学生")
kanji_field = u'Kanji'
# For cards within that query, this is the field that contains the English, and will be replaced by the dictionary definition (e.g. the field that contains "student")
meaning_field = u'Meaning'

#create soup of jmdict file
dict_doc = open(path_to_jmdict, 'r', encoding='utf-8')
soup = BeautifulSoup(dict_doc.read(), 'xml')
print("soup created")
dict = {}

#build python dictionary of words in jmdict using soup
#reformats the original definitions to be more concise
#e.g. 下水 with definition:
#   1. drainage; ditch; gutter; sewerage​
#   2. sewage; black water​
#becomes = drainage, ditch, gutter, sewerage​; sewage, black water​
for entry in soup.find_all("entry"):
    if entry.keb == None and entry.reb == None:
        continue
    else:
        dict_str = ""
        for sense in entry.find_all('sense'):
            sense_str = ""
            for gloss in sense.find_all("gloss"):
                sense_str += str(gloss.contents[0])
                sense_str += ", "
                sense_str = sense_str.replace(", to ", ", ")
            dict_str += sense_str[:-2] + "; "
        dict_str = dict_str[:-2]
    
        if (entry.keb != None):
            for keb in entry.find_all('keb'):
                if keb.contents[0] not in dict:
                    dict[keb.contents[0]] = dict_str
        if (entry.reb != None):
            for reb in entry.find_all('reb'):
                if reb.contents[0] not in dict:
                    dict[reb.contents[0]] = dict_str
print("built dict")

# Get the user's collection
cwd = os.getcwd()
col = Collection(collection_path)
os.chdir(cwd) # correct the cwd

#print info about collection to be processed
card_ids = col.findCards(search_query)
length = len(card_ids)
print("There are {} cards".format(length))
i = 0
last_card = ""
#start iterating over cards
for id in card_ids:
    i += 1
    print("{} out of {} cards".format(i,length))
    card = col.getCard(id)
    note = card.note()
    try:
        #get the stuff in the kanji field of anki card
        note_kanji = note[kanji_field]
        #remove some auxillary stuff, can remove the below code before strip
        note_kanji = re.sub('<[^<]+?>', '', note_kanji)
        note_kanji = re.sub('\.\.\.', '', note_kanji)
        note_kanji = note_kanji.strip()

        print(note_kanji)

        #skip if just processed (b/c multiple cards are mapped to one note)
        if (last_card == note_kanji):
            print("skipped")
            continue
        last_card = note_kanji

        #search dictionary for word, and if found change the Meaning field of the card, and tag as "AUTOMATED"
        #otherwise tag as "NOT FOUND"
        found = False
        if note_kanji not in dict:
            note.addTag("NOT_FOUND")
            note.flush()
            print("NOT FOUND")
        else:
            dict_str = dict[note_kanji]
            print(dict_str)
            note.addTag("AUTOMATED")
            note[meaning_field] = dict_str
            note.flush()
            print("MEANING SET")
        print("")
       
    except Exception as e:
        print(e)
        continue

col.close()
