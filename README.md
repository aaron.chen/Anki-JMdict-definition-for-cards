# Anki-JMdict-Definition-For-Cards
Quick and dirty script to add JMdict definitions to Japanese Anki Cards.

This script looks at the kanji field of an anki card, searches the JMdict file, and replaces the meaning field with the JMdict definition.

This script is not foolproof; if two words have the same kanji, but different entries in the dictionary (for example 角 [つの] and 角 [かど]), then it chooses the first one it finds. It can be changed to further narrow down the dictionary entry based on the reading, but this makes it so you can't put any notes in your "Reading" field, so I didn't do it.

In any case, all changed cards are tagged "AUTOMATED" and all unchanged cards are tagged "NOT_FOUND".

Before running the script, you must have first do several things

1. Make sure you have [the anki development files](https://github.com/dae/anki) so the script can import the anki python files and that this script is located in that repository's root folder.
2. Download the JMdict_e.gz file at the [JMDict homepage](http://www.edrdg.org/jmdict/edict_doc.html), and extract it.
3. Make sure that the relevant fields are specified within the script. Specifically change `path_to_jmdict`, `collection_path`, `search_query`, `kanji_field`, and `meaning_field` so that it works for your computer and anki cards. Details are shown in the comments
